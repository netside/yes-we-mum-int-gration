'use strict';
var projectName = 'front-ywm';

// MODULES
var gulp            = require('gulp');
var gutil           = require('gulp-util');
var $               = require('gulp-load-plugins')();
var browserSync     = require('browser-sync');
var reload          = browserSync.reload;
var browser_support = global.browser_support;

// PATHS
var basePaths = {
    src: 'src/',
    dist: 'dist/'
}

var path = {
    styles: {
        src: basePaths.src + 'scss/',
        dist: basePaths.dist + 'css/'
    },
    scripts: {
        src: basePaths.src + 'js/',
        dist: basePaths.dist + 'js/'
    },
    images: {
        src: basePaths.src + 'images/',
        dist: basePaths.dist + 'images/',
    },
    refresh: {
        src: basePaths.src,
        dist: basePaths.dist
    },
    fonts: {
        src: basePaths.src + 'fonts/',
        dist: basePaths.dist + 'fonts/'
    }
}

// Browser support
global.browser_support = [
    "ie >= 9",
    "ie_mob >= 10",
    "ff >= 30",
    "chrome >= 34",
    "safari >= 7",
    "opera >= 23",
    "ios >= 7",
    "android >= 4.4",
    "bb >= 10"
]

// TASKS

gulp.task('include', function() {
  return gulp.src([
      path.refresh.src + '*.html',
      !path.refresh.src + '/partials/*.html'
    ])
    .pipe($.fileInclude({ prefix: '@', basepath: '@file' }))
    .pipe(gulp.dest(path.refresh.dist))
    .pipe(reload({stream:true}));
});

gulp.task('styles', function () {
    return gulp.src(path.styles.src + '*.scss')
        .pipe($.plumber())
        .pipe($.sourcemaps.init())
        .pipe($.sass().on('error', $.sass.logError))
        .pipe($.autoprefixer({browsers: browser_support}))
        .pipe($.sourcemaps.write('.'))
        .pipe(gulp.dest(path.styles.dist))
        .pipe(reload({stream:true}));
});

gulp.task('scripts', function () {
  return gulp.src([
  		!path.scripts.src + '*.test.js',
  		path.scripts.src + '**/*.js'
  	])
    .pipe($.plumber())
    .pipe(gulp.dest(path.scripts.dist))
    .pipe(reload({stream:true}));
});

gulp.task('images', function () {
  return gulp.src(path.images.src + '**/*')
    .pipe($.newer(path.images.dist))
    .pipe($.imagemin({
      progressive: true,
      interlaced: true
    }))
    .pipe(gulp.dest(path.images.dist))
    .pipe(reload({stream:true}))
});

gulp.task('fonts', function() {
    return gulp.src(path.fonts.src + '**/*')
        .pipe(gulp.dest(path.fonts.dist))
        .pipe(reload({stream:true}));
});

gulp.task('clean', require('del').bind(null, ['dist','*.zip']));

gulp.task('build',['clean','include','styles','scripts','images', 'fonts']);

gulp.task('default',['build'], function() {
    browserSync.init({ server: { baseDir:  basePaths.dist }});

    gulp.watch(path.refresh.src + '**/*.html', ['include'])
    gulp.watch(path.scripts.src + '**/*.js', ['scripts'])
    gulp.watch(path.styles.src + '**/*.scss', ['styles'])
    gulp.watch(path.images.src + '**/*', ['images'])
});

// Compresse le dossier 'dist' dans un fichier .zip
gulp.task('zip', ['build'], function() {
    return gulp.src([
    	path.refresh.dist + '**/*',
    	!path.scripts.src + '*.test.js',
    	])
        .pipe($.zip(projectName + '.zip'))
        .pipe(gulp.dest('./'));
});
