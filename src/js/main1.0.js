(function($){
	/******************************************************************************************
		BLOC RECHERCHE
	******************************************************************************************/
	// Function
	function resetBlocSearch() {
		$('#search-advanced').show();
		$('#search-free').hide();
	}

	// Affichage du bouton reset pour les activitÃƒÂ©s/adresses
	var $inputRadio =  $('.checkbox-type').children('input[type=radio]');

	$inputRadio.click(function(event) {
		$('#reset-activity').slideDown();
	});

	$('#reset-activity').click(function(event) {
		$inputRadio.attr('checked', false);
		$('#reset-activity').delay(500).slideUp();
		event.preventDefault();
	});


	// Ouverture de la sidebar "Profil"
	$('#left-sidebar-btn').click(function(e) {
		$('body').toggleClass('is-open');
		$(this).toggleClass('is-open');
		//modif verifie etat sidebar
		setCookie("sidebar",document.getElementById('left-sidebar-btn').className.indexOf("is-open") > -1,90);
		e.preventDefault;
	});

	// Ouverture de la search box
	$('#search-box-link, .search-box--close, .bar-result__modify, .bar-result__container').click(function(e) {
		$('#search-box-link').toggleClass('is-active');
		$('#search-box').toggleClass('is-open');
		$('div.ym-container').toggleClass('is-masked');
		resetBlocSearch();

		e.preventDefault;
	});

	// Hide/show bloc recherche

	$('#search-free-btn').click(function() {
		$('#search-advanced').hide();
		$('#search-free').show();
	});

	$('#search-advanced-btn').click(function() {
		resetBlocSearch();
	});

	/******************************************************************************************
		BLOC SELECT ACTIVITE ADRESSES
	******************************************************************************************/
	$('#ym-select').click(function() {
		$('.ym-search__doing').toggleClass('is-expand');
	});

	$('.search-box__type input[type="radio"]').click(function() {
		$('#ym-doing__focus').removeClass();
		if ($(this).is(':checked')) {
			// modif : ajouter le cas ou je change dans le menu du haut et pas du bas
			var focusType = $('#label_'+ $(this).attr('id')).text();
			console.log(focusType);
			$('.ym-placeholder').remove();
			$('#ym-doing__focus').addClass('tag tag--' + focusType).text(focusType);

			/*setTimeout(function() {
				$('.ym-search__doing').removeClass('is-expand');
			}, 800)*/
		};
	});


	// Check/Uncheck radio
	$(".checkbox-type input[type='radio']").click(function() {
		var previousValue = $(this).attr('previousValue');
		var name = $(this).attr('name');

		if (previousValue == 'checked') {
			$(this).removeAttr('checked');
			$(this).attr('previousValue', false);
			$('#ym-doing__focus').removeClass().text('');
			$('.ym-placeholder').show();
			$('#reset-activity').delay(500).slideUp();
		} else {
			$("input[name=" + name + "]:radio").attr('previousValue', false);
			$(this).attr('previousValue', 'checked');
		}
		//modif
		$('#item_targets').val($('.search-box__type input[type="radio"]:checked').val());
		//getAdresses();
	});

	$('.rating-global').click(function(event) {
		var previousValue = $(this).attr('previousValue');
		var name = $(this).attr('name');

		if (previousValue == 'checked') {
			$(this).removeAttr('checked');
			$(this).attr('previousValue', false);
		} else {
			$("input[name=" + name + "]:radio").attr('previousValue', false);
			$(this).attr('previousValue', 'checked');
		}
	});

	$('.search-box__type').mouseleave(function() {
		$('.ym-search__doing').removeClass('is-expand');
	});

	/* *****************************************************************************************
		SELECT STYLE FILTER
	***************************************************************************************** */

	$('.ym-filter').fancySelect();
	// IncrÃƒÆ’Ã‚Â©mantation de la classe item (pour l'animation)
	$('.options').each(function(i) {
		$(this).children('li').each(function(j) {
			$(this).addClass('item item--' + (j+1));
		})
	});

	/* *****************************************************************************************
		ADD TO FAVORITE
	********************************************************************************************/

	$('.add-favorite__icon').click(function(e) {
		e.preventDefault();
		$(this).toggleClass('is-added');
		//modif
		pinItem($(this).attr('name'),'',$(this).hasClass('is-added'));
		e.preventDefault();
		e.stopPropagation();
	})

	/**
	 * Add/remove Friend & familly
	 * Gestion des ÃƒÆ’Ã‚Â©tats quand on ajoute ou suprime un amis et un membre de la famille
	 */

	var txtAddFriend = "Ajouter ÃƒÂ  mes amis";
	var txtSuprFriend = "Retirer de mes amis";
	var txtSuprFamilly = "Vous devez d'abord le retirer de votre famille";


	$('.add-friend').tooltipster({
		animation: 'grow',
		delay: 400,
		debug: true
	});

	if ( $('.ym-friend').hasClass('is-friend') ) {
		$('.ym-friend').next('.add-friend').tooltipster('content', txtSuprFriend);
	} else {
		$('.ym-friend').next('.add-friend').tooltipster('content', txtAddFriend);
	}

	// Au click sur le bouton 'Ajouter ÃƒÆ’Ã‚Â  mes amis'
	$('body').on('click', '.add-friend', function(e) {

		// Si ajouter ÃƒÆ’Ã‚Â  la famille impossible de retirer des amis
		if ($(this).parent('.toggle-friend').hasClass('is-familly') ){
			return false;
		}

		// On ajoute la class 'is-friend' ÃƒÆ’Ã‚Â  la div parente
		if(!$(this).parent('.toggle-friend').hasClass('is-friend' ) || confirm("On le retire de vos amis ?")){
			$(this).parent('.toggle-friend').toggleClass('is-friend');
			pinItem( '', $(this).parent('.toggle-friend').attr('name'), $(this).parent('.toggle-friend').hasClass('is-friend' ));
		}

		// Chgt du texte de la tooltip au click
		if ( $(this).parent('.toggle-friend').hasClass('is-friend') ) {
			$(this).tooltipster('content', txtSuprFriend);
		} else {
			$(this).tooltipster('content', txtAddFriend);
		}

		e.preventDefault();
	});

	// Au click sur le bouton 'Ajouter ÃƒÂ Ã‚Â  ma famille'
	$('body').on('click', '.add-familly', function(e) {

		// On ajout la class 'is-familly' ÃƒÂ Ã‚Â  la div parente
		if(!$(this).parent('.toggle-friend').hasClass('is-familly' ) || confirm("On le retire de votre famille ?")){
			$(this).parent('.toggle-friend').toggleClass('is-familly');
			if( $(this).parent('.toggle-friend').hasClass('is-familly' ))makeFamily($(this).parent('.toggle-friend').attr('name'),1);
			else makeFamily($(this).parent('.toggle-friend').attr('name'),0);
			e.preventDefault();
		}
	});

	$('.request-familly__accept').click(function (e) {
		e.preventDefault();
		$('.request-familly').parent('.toggle-friend').addClass('is-familly').delay(1000).slideUp().queue(function() {
			$('.request-familly').remove();
		});
		makeFamily($('.request-familly').parent('.toggle-friend').attr('name'),2);
	});

	$('.request-familly__reject').click(function (e) {
		if(confirm("Vous Ãªtes sur ?")){
			makeFamily($('.request-familly').parent('.toggle-friend').attr('name'),0);
			$('.request-familly').parent('.toggle-friend').remove();
			e.preventDefault();
		}
	});

	$('.request-familly__cancel').click(function (e) {

		if(confirm("On retire la demande ?")){
			makeFamily($(this).parent('.toggle-friend').attr('name'),0);
			$(this).parent('.toggle-friend').remove();
		/*$(this).parent('.request-familly').delay(1000).slideUp().queue(function() {
			$(this).remove();
		});*/

		e.preventDefault();
		}
	});



	/* *****************************************************************************************
		LAYOUT - SINGLE ADRESSE #2 : TAB SINGLE ADRESSE
	***************************************************************************************** */
	$('ul.ym-tabs').each(function() {
		var $active, $content, $links = $(this).find('a');

		$active = $($links.filter('[href="' + location.hash + '"]')[0] || $links[0]);
		$active.addClass('is-active');

		$content = $($active[0].hash);
		$content.addClass('is-animate');

		$links.not($active).each(function() {
			$(this.hash).hide();
		});

		// isotope();

		$(this).on('click', 'a', function(e) {
			$active.removeClass('is-active');
			$content.hide().addClass('is-animate');

			$active = $(this);
			$content = $(this.hash);

			$active.addClass('is-active');
			$content.show().addClass('is-animate');
			// isotope();

			return false;
		});
	});

	// Trigger pour ouvrir les tabs
	$('.adresse-comments__link a, .add-avis, .open-tab').click(function(e) {
		$('.tab-avis').trigger('click');
		$('html,body').animate({
			scrollTop: $(".ym-tab__avis").offset().top - 300},
		1000, $.bez([0.215, 0.61, 0.355, 1]));
		e.preventDefault();
	});

	/*$('.coming-soon__add-comment').click(function(e) {
		$('.tab-avis').trigger('click');
		e.preventDefault();
	});

	$('.coming-soon__add-pic').click(function(e) {
		$('.tab-pictures').trigger('click');
		e.preventDefault();
	});*/

	// Affiche un retour visuel dÃƒÆ’Ã‚Â¨s que input:file change
	$(".add-pic__return").hide();
	$(".add-pic__input").on( "change", function( event ) {
		var imgVal = $(".add-pic__input").val();
		$(".add-pic__return").show();
		$(".add-pic__file").text(imgVal);
	});

	/* *****************************************************************************************
		LAYOUT - SINGLE ADRESSE #3 : ACCORDION
	***************************************************************************************** */

	function ymAccordion () {
		$('.ym-accordion').on('click', '.ym-accordion__toggle', function(event) {
			event.preventDefault();
			if ( $(this).parent().hasClass('is-open')){
				$(this).parent().removeClass('is-open');
				$(this).next().slideUp(350);
			} else {
				$(this).parent().parent().find('.ym-accordion').removeClass('is-open');
				console.log($(this).parent().prev('.ym-accordion'));
				$(this).parent().parent().find('.coming-soon__activity').slideUp(350);
				$(this).parent().toggleClass('is-open');
				$(this).next().slideToggle(350);
			}
		});
	}

	ymAccordion();

	$('.todo-activity__cover').click(function() {
		$('#activity-1').trigger('click');
	})


/*	function closeAccordion() {
		$('.coming-soon__resume').removeClass('is-toggle');
		$('.coming-soon__activity').slideUp(600).removeClass('is-toggle');
	}

	$('.ym-accordion').on('click', '.ym-accordion__toggle', function(e) {
		var currentAttrValue = $(this).attr('href');

		if ( $(e.target).is('.is-toggle') ) {
			closeAccordion();
		} else {
			closeAccordion();
			$(this).addClass('is-toggle');
			$(currentAttrValue).slideDown(600).addClass('is-toggle');
		}

		e.preventDefault();
	}).on('click', 'span', function(e) {
		e.stopPropagation();
	}).on('click', '.close-activity', function(e) {
		closeAccordion();
	});*/

	/* *****************************************************************************************
		LAYOUT - PAGE EN CE MOMENT
	***************************************************************************************** */
	$('.ym-todo__activities').mixItUp({
		layout: { display: 'block'},
		load: { filter: '.todo--week-end'}
	});

	$('#mixit-share').mixItUp({
		layout: { display: 'block'},
		selectors : {
			target: '.mix-share',
			filter: '.filter-friend'
		},
		load: { filter: '.my-friends'}
	});

	// SCROLL TO TOP
	 $(window).scroll(function () {
		if ($(this).scrollTop() > 500) {
			$('.ym-scroll-top').addClass('is-visible');
		} else {
			$('.ym-scroll-top').removeClass('is-visible');
		}
	});

	$('.ym-scroll-top').click(function () {
		$("html, body").animate({
			scrollTop: 0
		}, 600);
		return false;
	});

	/* *****************************************************************************************
		LAYOUT - SINGLE ARTICLE
	***************************************************************************************** */
	$(window).scroll(function() {
		var scroll  = $(window).scrollTop();
		var $cover   = $('#article-cover');
		var $article   = $('#single-article');

		if (scroll >= 100) {
			$cover.addClass('is-animate');
			$article.addClass('is-animate');
		}
		else {
			$cover.removeClass('is-animate');
			$article.removeClass('is-animate');
		}
	});

	$('#widget-carousel').owlCarousel({
		singleItem: true,
		autoPlay: false,
		slideSpeed: 800,
		navigation: false,
		pagination: true,
	});

	/* *****************************************************************************************
		LAYOUT - LANDING PAGE (PRE-HOME)
	***************************************************************************************** */
	$('#testimonial-slider').owlCarousel({
		items: 4,
		autoPlay: false,
		slideSpeed: 800
	});

	/* *****************************************************************************************
		MODULE DE NOTIFICATION
	***************************************************************************************** */
	// Facultatif -> Permet la preview des notif
	$('.open-notif').click(function(event) {
		$('.notification').addClass('is-visible');
		$('header.ym-container').addClass('has-notification');
	});


	$('.notification').on('click', '.notification__close', function(event) {
		event.preventDefault();
		$('header.ym-container').removeClass('has-notification');
		$('.notification').removeClass('is-visible');
	});

/* *****************************************************************************************
		LAYOUT - SINGLE ADRESSE #1 : SLIDER SINGLE ADRESSE
	***************************************************************************************** */
	var owlContainer	= $('.adresse-pictures');
	var owlItems		= owlContainer.children('div');

	owlContainer.owlCarousel({
		singleItem: true,
		autoPlay: true,
		slideSpeed: 600,
		navigation: true,
		navigationText: ['<i></i>', '<i></i>'],
		pagination: false,
		transitionStyle : "backSlide",
		afterInit: function() {
			if (owlItems.length == 1) {
				owlContainer.stop();
			}
		}
	});

	$('.tooltip').tooltipster();
	$('.lightbox').featherlight();

	/**
	 * Module de partage
	 */

	$('.add-share').click(function(event) {
		event.preventDefault();
		$('.share-box').toggleClass('is-open');
	});

	$(".share-box").mouseleave(function(){
		setTimeout(function() {
			$('.share-box').removeClass('is-open');
		}, 800);
	});

	$('#show-mail').click(function(event) {
		$('.share-box__container').addClass('is-sliding');
	});

	$('.share__return').click(function(event) {
		$('.share-box__container').removeClass('is-sliding')
	});

	$('.radio-rating__reset').click(function(event) {
		if ($(this).hasClass('checked')) {
			$(this).removeProp('checked').removeClass('checked');
		}
		else {
			$(this).addClass('checked');
		}
	});


})(jQuery);
